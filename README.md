# Demo

## Prerequisites
- in case we are in an air-gapped environment (without internet), let's build an customized image with `openjdk:8-alpine` as base image, then install `tzdata` on top of it, we name it `openjdk:8-alpine-with-tzdata`
  ```bash
  cat Dockerfile.alpine-with-tzdata | docker build -t openjdk:8-alpine-with-tzdata -
  ```
- save the image to a tar archive
  ```bash
  docker save -o openjdk-8-alpine-with-tzdata.tar openjdk:8-alpine-with-tzdata
  ```
- now upload the tar file into server, then load it into its docker engine
  ```bash
  docker load -i openjdk-8-alpine-with-tzdata.tar
  ```
## Basic flow
- build the jar package
  ```bash
  mvn clean package
  ```
- build the docker image
  ```bash
  docker build -t timezone-with-alpine .
  ```
- run the container with our timezone (already set with `TZ` environment variable inside `Dockerfile`)
  ```bash
  docker run --rm timezone-with-alpine
  ```
- run the container with other timezone using TZ environment variable (can be set with `docker-compose` also)
  ```bash
  docker run --rm -e TZ=Asia/Singapore timezone-with-alpine
  ```
- first time run with docker-compose
  ```bash
  docker-compose up -d
  ```
## Change the timezone with docker-compose services
- update timezone, e.g. `America/New_York`
  ```yaml
  environment:
    TZ: 'America/New_York'
  ```
- recreate containers with docker-compose (because service's config is changed, it will pickup the changes and recreate the containers)
  ```bash
  docker-compose up -d
  ```
