package hino;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class MainApplication {

  public static void main(String[] args) {
    SpringApplication.run(MainApplication.class, args);
    log.info("Current timezone: {}", Calendar.getInstance().getTimeZone().getID());
    Timer timer = new Timer();
    timer.schedule(new SayHello(), 0, 2000);
  }

  static class SayHello extends TimerTask {

    @Override
    public void run() {
      log.info("Hello there!");
    }
  }
}
